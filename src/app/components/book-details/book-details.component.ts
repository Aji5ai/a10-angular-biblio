import { Component, Input, OnInit } from '@angular/core';
import { BookApiService } from '../../services/book-api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../../models/book.model';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrl: './book-details.component.scss',
})
export class BookDetailsComponent implements OnInit {
  @Input() viewMode = false;

  @Input() currentBook: Book = {
    title_suggest: '',
    cover_i: 0,
    first_publish_year: 0,
    number_of_pages_median: 0,
    ratings_average: 0,
  };

  message = '';

  constructor(
    private bookApiService: BookApiService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (!this.viewMode) {
      this.message = '';
      this.getBook(this.route.snapshot.params['cover_i']); // ou id ?
    }
  }

  getBook(id: string): void {
    this.bookApiService.get(id).subscribe({
      next: (data) => {
        this.currentBook = data;
        console.log(data);
      },
      error: (e) => console.error(e),
    });
  }
}
