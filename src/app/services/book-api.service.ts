import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Book } from '../models/book.model';

const apiUrl = 'https://openlibrary.org/search.json?author=king';

@Injectable({
  providedIn: 'root',
})
export class BookApiService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Book[]> {
    return this.http
      .get<{ docs: Book[] }>(apiUrl)
      .pipe(map((response) => response.docs));
  }

  // ne marche pas car pas de id associé aux livres
  get(id: any): Observable<Book> {
    return this.http.get<Book>(`${apiUrl}/${id}`);
  }
}
