export class Book {
  title_suggest?: string;
  cover_i?:number;
  first_publish_year?: number;
  number_of_pages_median?: number;
  ratings_average?: number;
}
